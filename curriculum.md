These are editor notes, they are not intended to be the workshop material.

Read the README.md file if you're here to learn. Unless that's empty, then use this.

# Installation

Most unix-like operating systems use this as their default shell, open a terminal and enter `echo $0`

If this does not return `bash`, call for help.

The terminal should be found as an already installed program / app.

For windows users, see if Bash on Ubuntu on windows is available for you (windows subsystem for linux).
https://docs.microsoft.com/en-us/windows/wsl/install-win10
https://youtu.be/Cvrqmq9A3tA

If that is not an option, Git Bash may be viable.

Otherwise, install and use PuTTY to connect to a linux machine via ssh.

# What is Bash

Bash is a command-line interpreter, also known as a shell.

The main goal today is to give you an idea of how to use it and what you can do with it.
Not to teach you how to do anything, but to give you enough confidence to try anything,
and to know what to search for when that doesn't work.

A central concept in all these operating systems is the "unix philosophy" of modularity.

Have small, purpose build programs, (which take data in from "stdin" perform some (fairly simple) operation
and direct their output to "stdout")

and link them together with the shell to perform more complex actions.

This is the primary purpose of a shell.

# Uses

who	| anyone
--------|--------
what	| glue operations
where	| unix-like systems
when	| to infinity and beyond
why	| ubiquity, efficiency

As an interactive shell, Bash is most useful for remote use via SSH where the computer isn't physically reachable (server), or does not have appropriate peripherals (raspberry pi).

Locally it is often useful for doing specific things which GUI tools are lacking, using tools lacking a GUI, or leveraging your experience from remote operations.

# Using Bash

When you open a terminal, it'll open Bash for you, which presents you with readline (man readline, 'nuf said).

This is an interactive session and starts in your home directory.

Here, you can simply type the name of a program and it will run.

example: `ls`

most programs have options

example: `ls -a`

by convention, single letter options use a single hyphen and can have multiple options for one hyphen

`ls -lah` (long format, all, human readable)

and multi letter options use double hyphens

`ls --version` (show version)

This convention is not set in stone.

## Some basic utilities

	cd, cd .. 			(. is the current directory, .. is the parent dir, ../.. is 2 levels up, ~ is the home dir)
	pwd					(print working directory)
	ls					(lists the contents of the current directory)
	cat					(concatenate files and print to stdout (can take 1 file)) 
	echo				(write arguments to stdout)
	whatis [program-name]	(show a one-line description of that program)
	man [program-name] 	(show the manual for a command)
	apropos [search term]	(search in all man pages)
	file [filename] 	(show information about the file)

look around a bit, ask questions

By now you're probably tired of typing everything out.

## Shortcuts

`^` or `C-` can mean `Ctrl`

	^+Shift+C 	(copy)
	^+Shift+V 	(paste)
	^+C			(exit program)
	^+D			(exit shell)
	^+A			(go to start of line)
	^+E			(go to end of line)
	^+U			(clear the entire line (works for password entry too))
	[up arrow] 		(go back through history)
	^+R			(search back through history)))))
	[tab]			(tab complete)
	!!			(last command, e.g.: sudo !!)

Tab completion is the completion of any commands by unambiguous parts.

When pressing the tab key on a partially complete command Bash will fill in what it can.

If there is ambiguity, it will do nothing, until you press tab a second time, then you are shown the options.

## Pipes

The `|` operator links together the stdout of the left program and the stdin of the right.

This structure is called a pipe, and can end on any program, causing the stdout of that program to be shown on screen.

If you want to save that output, you can pipe it to a file with the `>` symbol (`>>` appends to a file).

For example, to do a word frequency count.

First we `cat [filename]` followed by a pipe (`|`) to get the file into the stdin of the next program.

Now we convert everything to lower case as to not count upper and lower case separately.

`tr A-Z a-z` translates every character that is in the range A-Z to its equivalent position in the range a-z.

Then we want to remove all punctuation and separate words.

`tr --complement --squeeze-repeats a-z "\n"` translates everything that is the complement of a-z (not a character in the range a-z) to a newline and squeezes all repeated newlines into one.

(--complement uses the first given pattern and --squeeze-repeats uses the second, which is perfect for this application)

Now we want to sort the result of that because the program after it needs a sorted input for the behaviour we want.

`sort`

And then we want to count the occurences of each unique line. (uniq (yes they saved a few letters) removes matching adjacent lines)

`uniq --count`

Finally, we use sort again, this time sort in reverse order, by number.

`sort --reverse --numeric-sort`

To save this into a file, we use the `>` operator and specify a file (this file gets created if it doesn't exist).

`> freqcounted`

Files in unix-like systems don't require an extension, it is only there for readability and to facilitate things like globbing. The extension is purely a part of the filename.

To do all of this at once, we can put those commands into a file and have Bash run that.

# Scripts

In the spirit of modularity, one usually tries to avoid making scripts file specific, therefore we're not going to include the `cat` and `>` commands in our script.

Make a file called "freqcount.sh" put the following in it:

```bash
#!/usr/bin/env bash

tr A-Z a-z | tr -cs a-z "\n" | sort | uniq -c | sort -rn

```

This is a Bash script, the first line (called a hash-bang or shebang) tells the computer what to run this file with (in this case, Bash).

The rest of the file can be seen as being typed into the prompt automatically.


To use this script, we must make it executeable `chmod +x freqcount.sh`.

Then we can simply `cat bash-ref.txt | ./freqcount.sh > freqcounted`

`./` gives bash the path to the script to execute.

# Globbing

`.` as a starting character has to be explicitly stated (?bashrc won't work)

`*` represents any unspecified characters (down to 0 characters)

`**` represents any characters, recursively (matches dir/dir/dir/dir/dir/dir/dir/file etc)

`?` represents a single unspecific character

`[ ]` characters in braces are specific

`^` means logical NOT, e.g.: `[^a-c]*` matches any files that do not start with `a`, `b` or `c`

# More utilities

> Every one was written first for a particular need, but untangled from the specific application.

note: "file" here can usually mean stdin if no file has been specified as an argument

also: "print" here means output to stdout, which ends up on screen if you're not piping it to another program

## String manipulation
	grep (print all lines in given file that contain a match to the given pattern)
	head (print the first 10 lines of a file)
	tail (print the last 10 lines of a file)
	less (open a file in more and vi style read-only, space to page down (how to go up?), j and k for down and up scrolling, u and d for up and down jumping)
	tr (translates characters matching the first given pattern to the second pattern)
	strings (print printable strings of 4 or more characters from a file)
	sort (print sorted content of provided file(s))
	uniq (remove duplicate adjacent lines (consider sorting before this), outputs to stdout)
	wc (print newline, word, and byte counts for each file)
	sed (stream editor, learn regex)
	awk (pattern-action record manipulating program with its own language)
	nano (simple editor from GNU)
	vi (:q to exit, but if you're using it, come to our vim workshop instead)
	emacs (seriously, what are you doing here?)

## Administration
	sudo (execute the following as root (requires password and the current user to be on the sudoer list))
	chmod (change file mode bits, e.g.: read, write, execute)
	top (displays a real time view of running processes ranked by cpu usage (highest usage at the top))
	whoami (prints current user's username)
	w (shows who's logged in, when they logged in, etc)
	id (shows given user's permissions (current user if none is given))

## File manipulation
	ls (lists the contents of the current directory, -a to also list .files)
	find ¯\_(ツ)_/¯
	realpath (shows the full path of a file)
	cp (copy file(s) to path)
	mv (moves file(s) to path)
	rm (removes file(s), FEAR ME!, NO UNDO!, -rf for directories)
	touch (updates last modified time, creates file if it doesn't exist)
	cat (concatenates file(s) to stdout)
	tac (concatenates file(s) to stdout in reverse order (last line first))
	mkdir (makes a directory)
	sha1sum (calculate the sha1 hash of a file sha256, 512, etc also possible)
	stat (displays file status, size, last modified, etc)
	unexpand (convert spaces to tabs)

## Pipe manipulation
	tee (write stdin to stdout and specified file(s))

## Environment
	info (modern alternative to man)
	pwd (print working directory)
	time (times the following command)
	timeout (runs the following command with a specified timeout)
	sleep (do nothing for specified time)
	watch (run the following command every 2s (other times possible, -n))
	printenv (print out the current environmental variables)
	uptime (show machine uptime)
	history (shows command history)

## Disk manipulation
	df (show amount disk free, try -h option for human readable)
	du (show amount disk used, try -h option for human readable)

here be dragons!

	dd (manipulates raw data, can mess things up if not used carefully, read the manual (man dd))
	fdisk (may not be installed, disk partition table manipulation, also dangerous, though fdisk -l just shows lists all the partitions)


# Programs as arguments

sudo, watch, time, xarg, etc

for example: `time (sudo dmesg | tail)`

# Job control

ctrl-z, jobs, bg, fg, &, &!

# Command substitution

Using the output of a command as a variable.

	$(command (sub-command) )
	echo "$(sudo dmesg)" | tail

note: `$((expression))` is arithmetic expansion, it evaluates the expression and returns the result. (don't use `((` unless you want this)

# Customization

.bashrc
aliasses

# Alternatives

sh (usually the system default shell in POSIX mode)

zsh (extended bourne shell with many extra features)

not fish, anything but fish! (it's not POSIX compliant)

# References

https://www.gnu.org/software/bash/manual/bashref.html

http://wiki.bash-hackers.org/

http://wiki.bash-hackers.org/scripting/tutoriallist

## Advanced Bash

https://youtu.be/uqHjc7hlqd0
